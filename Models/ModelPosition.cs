﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreAgs.Models
{
    public class ModelPosition
    {
        [Required]
        public string? Name { get; set; }
        [Required]
        public int? CreatedBy { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
