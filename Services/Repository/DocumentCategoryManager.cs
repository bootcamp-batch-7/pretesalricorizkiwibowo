﻿using Dapper;
using PretestCoreAgs.Models;
using System.Data.SqlClient;
using System.Data;
using PretestCoreAgs.Services.Interface;

namespace PretestCoreAgs.Services.Repository
{
    public class DocumentCategoryManager : IDocumentCategoryManager
    {
        private readonly IConfiguration _configuration;
        public DocumentCategoryManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Response<T> Execute_Command<T>(string query, DynamicParameters sp_params)
        {
            Response<T> response = new Response<T>();

            using (IDbConnection dbConnection = new SqlConnection(_configuration.GetConnectionString("default")))
            {
                if (dbConnection.State == ConnectionState.Closed)
                    dbConnection.Open();

                using var transaction = dbConnection.BeginTransaction();

                try
                {
                    response.Data = dbConnection.Query<T>(query, sp_params, commandType: CommandType.StoredProcedure, transaction: transaction).FirstOrDefault();
                    response.Code = sp_params.Get<int>("retVal"); //get output parameter value
                    response.Message = "Success";
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Code = 500;
                    response.Message = ex.Message;
                }
            }

            return response;
        }
        public Response<List<T>> getDocumentCategoryList<T>()
        {
            Response<List<T>> response = new Response<List<T>>();
            using IDbConnection db = new SqlConnection(_configuration.GetConnectionString("default"));
            string query = "select * from DocumentCategory";

            try
            {
                response.Data = db.Query<T>(query, null, commandType: CommandType.Text).ToList();
                response.Code = 200;
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
            }

            return response;
        }
        public Response<ModelDocumentCategory> GetDocumentCategoryById(string id)
        {
            Response<ModelDocumentCategory> response = new Response<ModelDocumentCategory>();
            using IDbConnection db = new SqlConnection(_configuration.GetConnectionString("default"));
            string query = "SELECT * FROM DocumentCategory WHERE ID = @Id";

            try
            {
                response.Data = db.Query<ModelDocumentCategory>(query, new { Id = id }, commandType: CommandType.Text).FirstOrDefault();
                response.Code = 200;
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
