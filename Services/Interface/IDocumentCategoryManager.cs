﻿
using Dapper;
using PretestCoreAgs.Models;

namespace PretestCoreAgs.Services.Interface
{
    public interface IDocumentCategoryManager
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getDocumentCategoryList<T>();
        Response<ModelDocumentCategory> GetDocumentCategoryById(string id);
    }
}
