﻿using Dapper;
using PretestCoreAgs.Models;

namespace PretestCoreAgs.Services.Interface
{
    public interface ICompanyManager
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getCompanyList<T>();
        Response<ModelCompany> GetCompanyById(string id);
    }
}
