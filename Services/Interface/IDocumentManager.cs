﻿
using Dapper;
using PretestCoreAgs.Models;

namespace PretestCoreAgs.Services.Interface
{
    public interface IDocumentManager
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getDocumentList<T>();
        Response<ModelDocument> GetDocumentById(string id);
    }
}
