﻿using Dapper;
using PretestCoreAgs.Models;

namespace PretestCoreAgs.Services.Interface
{
    public interface IPositionManager
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getPositionList<T>();
        Response<ModelPosition> GetPositionById(string id);
    }
}
