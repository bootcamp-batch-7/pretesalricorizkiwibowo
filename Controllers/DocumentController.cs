﻿using Dapper;
using PretestCoreAgs.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using PretestCoreAgs.Services.Interface;
using System.Reflection.Metadata;
using System.Xml.Linq;

namespace PretestCoreAgs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IDocumentManager _authentication;
            public DocumentController(IDocumentManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [AllowAnonymous]
        [Authorize(Roles = "Admin")]
        public  IActionResult Create([System.Web.Http.FromBody] ModelDocument document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", document.IDCompany);
            dp_param.Add("idcategory", document.IDCategory);
            dp_param.Add("name", document.Name);
            dp_param.Add("description", document.Description);
            dp_param.Add("flag", document.Flag);
            dp_param.Add("createdby", document.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_createDocument", dp_param);
            if(result.Code == 200)
            {
                return Ok(new { data = document, message = "Success", code = 200 });
            }
            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("Get")]
        [Authorize(Roles = "Admin")]
        public IActionResult Get()
        {
            var result = _authentication.getDocumentList<ModelDocument>();
            return Ok(result);
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "Admin")]
        [AllowAnonymous]

        public IActionResult GetID(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("Parameter is missing");
            }

            var response = _authentication.GetDocumentById(id);

            if (response.Code == 200)
            {
                return Ok(response.Data);
            }
            else
            {
                return StatusCode(response.Code, response.Message);
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument document, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter invalid");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("idcompany", document.IDCompany);
            dp_param.Add("idcategory", document.IDCategory);
            dp_param.Add("name", document.Name);
            dp_param.Add("description", document.Description);
            dp_param.Add("flag", document.Flag);
            dp_param.Add("createdby", document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelDocument>("sp_updateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = document, message = "Success", code = 200 });
            }

            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_deleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }

    
}
