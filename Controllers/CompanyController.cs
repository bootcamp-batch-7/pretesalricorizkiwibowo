﻿using Dapper;
using PretestCoreAgs.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using PretestCoreAgs.Services.Interface;

namespace PretestCoreAgs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : Controller
    {
        private readonly ICompanyManager _authentication;
            public CompanyController(ICompanyManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [AllowAnonymous]
        [Authorize(Roles = "Admin")]
        public  IActionResult CreateCompany([System.Web.Http.FromBody] ModelCompany company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", company.Name);
            dp_param.Add("address", company.Address);
            dp_param.Add("email", company.Email);
            dp_param.Add("telephone", company.Telephone);
            dp_param.Add("flag", company.Flag);
            dp_param.Add("createdby", company.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_createCompany", dp_param);
            if(result.Code == 200)
            {
                return Ok(new { data = company, message = "Success", code = 200 });
            }
            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("Get")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCompany()
        {
            var result = _authentication.getCompanyList<ModelCompany>();

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Admin")]
        [AllowAnonymous]

        public IActionResult GetID(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("Parameter is missing");
            }

            var response = _authentication.GetCompanyById(id);

            if (response.Code == 200)
            {
                return Ok(response.Data);
            }
            else
            {
                return StatusCode(response.Code, response.Message);
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [AllowAnonymous]
        public IActionResult updateCompany([System.Web.Http.FromBody] ModelCompany company, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter invalid");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.String);
            dp_param.Add("createdby", company.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelCompany>("sp_updateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = company, message = "Success", code = 200 });
            }

            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_deleteCompany", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }

    
}
