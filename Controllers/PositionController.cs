﻿using Dapper;
using PretestCoreAgs.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using PretestCoreAgs.Services.Interface;

namespace PretestCoreAgs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : Controller
    {
        private readonly IPositionManager _authentication;
            public PositionController(IPositionManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [AllowAnonymous]
        [Authorize(Roles = "Admin")]
        public  IActionResult Create([System.Web.Http.FromBody] ModelPosition position)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", position.Name);
            dp_param.Add("createdby", position.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_createPosition", dp_param);
            if(result.Code == 200)
            {
                return Ok(new { data = position, message = "Success", code = 200 });
            }
            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("Get")]
        [Authorize(Roles = "Admin")]
        public IActionResult Get()
        {
            var result = _authentication.getPositionList<ModelPosition>();

            return Ok(result);
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "Admin")]
        [AllowAnonymous]

        public IActionResult GetID(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("Parameter is missing");
            }

            var response = _authentication.GetPositionById(id);

            if (response.Code == 200)
            {
                return Ok(response.Data);
            }
            else
            {
                return StatusCode(response.Code, response.Message);
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [AllowAnonymous]
        public IActionResult Update([System.Web.Http.FromBody] ModelPosition position, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter invalid");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("name", position.Name, DbType.String);
            dp_param.Add("createdby", position.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelPosition>("sp_updatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = position, message = "Success", code = 200 });
            }

            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_deletePosition", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }

    
}
